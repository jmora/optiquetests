lazy val personalSettings = Seq(
  organization := "eu.optique",
  version := "0.0.1",
  scalaVersion := "2.11.5" 
)

lazy val mainapp = Some("eu.optique.jmora.mapping.analysis.test.App")
lazy val maintest = Some("eu.optique.jmora.mapping.analysis.test.OptiqueSpec")

lazy val preferredSettings = Seq(
    // pollInterval := 1000,
    javacOptions ++= Seq("-source", "jvm-7", "-target", "jvm-7"),
    scalacOptions += "-deprecation",
    scalaSource in Compile := baseDirectory.value / "src" / "main",
    scalaSource in Test := baseDirectory.value / "src" / "test",
    // watchSources += baseDirectory.value / "input",
    ivyLoggingLevel := UpdateLogging.Full,
    // javaOptions += "-Xmx4G",
    shellPrompt in ThisBuild := { state => Project.extract(state).currentRef.project + "> " },
    shellPrompt := { state => System.getProperty("user.name") + "> " }
)

lazy val root = (project in file(".")).
  settings(personalSettings: _*).
  settings(preferredSettings: _*).
  settings(
    name := "jmora.mapping.analysis.test",
    libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.4" % Test,    
    mainClass in (Compile, packageBin) := mainapp,
    mainClass in (Compile, run) := mainapp,
    mainClass in (Test, run) := maintest
  )
