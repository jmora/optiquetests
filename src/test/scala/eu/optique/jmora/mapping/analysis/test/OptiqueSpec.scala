package eu.optique.jmora.mapping.analysis.test

import org.scalatest.Suites

class OptiqueSpec extends Suites(
  new SyntaxAnalysisSpec,
  new SemanticAnalysisSpec
)
