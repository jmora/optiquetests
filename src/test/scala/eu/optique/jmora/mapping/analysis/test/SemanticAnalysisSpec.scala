package eu.optique.jmora.mapping.analysis.test

import org.scalatest.WordSpec
import eu.optique.jmora.mapping.analysis.test.HelpingDefs._
import eu.optique.api.component.omm.analysis.interfaces.optique.BatchAnalyser
import scala.collection.JavaConversions._
import eu.optique.api.component.omm.analysis.MappingAnalysisRelation._

class SemanticAnalysisSpec extends WordSpec {

  val rawResults = BatchAnalyser.analyse(
    "resources/wellOntology20141019.ttl",
    "resources/wellMappings20141019.ttl",
    "resources/statoilSchema-Simple.schema"
  )
  // val results0 = rawResults.values.flatMap(_.flatMap(_.map(_.toNamedString))).toSeq

  implicit val results = for (
    analysisLevel <- rawResults.values.toSeq;
    mappingAssertion <- analysisLevel;
    result <- mappingAssertion
  ) yield { result.toNamedString }
  results foreach println

  val caseSensitive = !eu.optique.api.component.omm.analysis.convenience.Globals.caseinsensitiveness
  val case_sensitive = if (caseSensitive) "case sensitive" else "case insensitive"

  "The full analysis results" when {
    s"analysis is $case_sensitive" should {
      "find head inconsistencies" in {
        assert("node19740jfkdx285" hasProblem HEAD_INCONSISTENT)
        assert("node19740jfkdx72" hasProblem HEAD_INCONSISTENT)
        assert("node19740jfkdx78" hasProblem HEAD_INCONSISTENT)
        assert("node19740jfkdx345" hasProblem HEAD_INCONSISTENT)
        assert("node19740jfkdx360" hasProblem HEAD_INCONSISTENT)
        assert("node19740jfkdx249" hasProblem HEAD_INCONSISTENT)
        assert("node19740jfkdx264" hasProblem HEAD_INCONSISTENT)
        assert("node19740jfkdx309" hasProblem HEAD_INCONSISTENT)
        assert("node19740jfkdx201" hasProblem HEAD_INCONSISTENT)
        assert("node19740jfkdx27" hasProblem HEAD_INCONSISTENT)
        assert("node19740jfkdx330" hasProblem HEAD_INCONSISTENT)
        assert("node19740jfkdx516" hasProblem HEAD_INCONSISTENT)
        assert("node19740jfkdx30" hasProblem HEAD_INCONSISTENT)
        assert("node19740jfkdx48" hasProblem HEAD_INCONSISTENT)
        assert("node19740jfkdx54" hasProblem HEAD_INCONSISTENT)
        assert("node19740jfkdx438" hasProblem HEAD_INCONSISTENT)
      }

    }
  }
}
