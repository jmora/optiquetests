package eu.optique.jmora.mapping.analysis.test

import org.scalatest.WordSpec
import eu.optique.api.component.omm.analysis.interfaces.optique.TerminalHelper
import org.openrdf.model.impl.URIImpl
import eu.optique.api.component.omm.analysis.syntax.SyntacticAnalyserImpl
import scala.collection.JavaConversions._
import eu.optique.api.component.omm.analysis.sqlapi.SQLPartialAPI
import eu.optique.api.component.omm.analysis.syntax.BodyChecker
import java.util.ArrayList
import eu.optique.api.mapping.TriplesMap
import scala.util.{ Try, Success, Failure }
import scala.collection.parallel.ParSeq
import eu.optique.jmora.mapping.analysis.test.HelpingDefs._
import eu.optique.api.component.omm.analysis.MappingAnalysisRelation
import eu.optique.api.component.omm.analysis.MappingAnalysisRelation._
import eu.optique.api.component.omm.analysis.syntax.FrontierChecker
import java.util.HashSet
import eu.optique.api.component.omm.analysis.MappingAnalysisOrderedResult
import eu.optique.api.component.omm.analysis.impl.MappingAnalysisOrderedSingleResult
import eu.optique.api.component.omm.analysis.syntax.HeadChecker

class SyntaxAnalysisSpec extends WordSpec {
  val helper = new TerminalHelper
  val mappings: ArrayList[TriplesMap] = new ArrayList[TriplesMap](helper getMappings "resources/mappings.ttl")
  val metaDataURI = new URIImpl("http://www.optique-project.eu/mapping-analysis/base/")
  val schema = helper.getSchema("resources/databaseSchema.rdf", metaDataURI)
  val signature = SyntacticAnalyserImpl getSignature schema
  val ontology = helper getOntology "resources/ontology.ttl"
  val osignature = SyntacticAnalyserImpl getSignature ontology

  val preliminaryChecks = mappings.par.map(x =>
    Try { new SQLPartialAPI(x) }
  ).zipWithIndex
  val preliminaryBodyChecks = preliminaryChecks.map {
    case (Success(spapi), i) => Try { BodyChecker.check(mappings, i, signature, spapi) }
    case (Failure(e), _)     => Failure(e)
  }.seq
  val bodyChecks = getActual(preliminaryBodyChecks)
  val preliminaryFrontierChecks = preliminaryChecks.seq.map {
    case (Success(spapi), i) => Try {
      FrontierChecker.check(mappings, i, signature,
        new BodyChecker(spapi, signature, new HashSet[Integer], mappings))
    }
    case (Failure(e), _) => Failure(e)
  }
  val frontierChecks = getActual(preliminaryFrontierChecks)
  val preliminaryHeadChecks = (0 until mappings.size).map(
    i => Try { HeadChecker.check(mappings, i, osignature) }
  ).seq
  val headChecks = getActual(preliminaryHeadChecks)
  val caseSensitive = !eu.optique.api.component.omm.analysis.convenience.Globals.caseinsensitiveness
  val case_sensitive = if (caseSensitive) "case sensitive" else "case insensitive"

  def getActual(checks: Seq[Try[ArrayList[MappingAnalysisOrderedSingleResult]]]) =
    checks.flatMap(x => if (x.isSuccess) x.get.map(_.toNamedString) else Seq())

  "Syntactic body analysis" when {
    val problems = if (caseSensitive) 5 else 4
    implicit val problemStrings = bodyChecks

    case_sensitive should {

      "work without exceptions" in {
        assert(preliminaryBodyChecks.forall(_.isSuccess))
      }

      "find undefined variables" in {
        if (caseSensitive)
          assert(19 hasProblem UNDEFINED_VARIABLE forElements "p m")
        assert(17 hasProblem UNDEFINED_VARIABLE forElements "e")
        assert(22 hasProblem UNDEFINED_VARIABLE forElements "h")
      }
      "find ambiguous variables" in {
        assert(11 hasProblem AMBIGUOUS_VARIABLE forElements "d c")
        assert(17 hasProblem AMBIGUOUS_VARIABLE forElements "e")
      }
      s"find exactly $problems problems" in {
        assert(bodyChecks.size == problems)
      }
    }
  }

  "Syntactic frontier analysis" when {
    val problems = if (caseSensitive) 3 else 2
    implicit val problemStrings = frontierChecks
    case_sensitive should {
      "work without exceptions" in {
        assert(preliminaryFrontierChecks.forall(_.isSuccess))
      }
      "find no grounded variables" in {
        if (caseSensitive)
          assert(18 hasProblem VARIABLE_NOT_GROUNDED forElements "P h")
        assert(23 hasProblem VARIABLE_NOT_GROUNDED forElements "c")
        assert(24 hasProblem VARIABLE_NOT_GROUNDED forElements "w")
      }
      s"find exactly $problems problems" in {
        assert(frontierChecks.size == problems)
      }
    }
  }

  "Syntactic head analysis" when {
    val problems = 2
    implicit val problemStrings = headChecks
    case_sensitive should {
      "work without exceptions" in {
        assert(preliminaryHeadChecks.forall(_.isSuccess))
      }
      "find not defined classes" in {
        assert(25 hasProblem CLASS_NOT_DEFINED forElements "http://optique-project.eu/mappings/analysis/test/ontology/RA")
      }
      "find not defined properties" in {
        assert(27 hasProblem PROPERTY_NOT_DEFINED forElements "http://optique-project.eu/mappings/analysis/test/ontology/asdfg")
      }
      s"find exactly $problems problems" in {
        assert(frontierChecks.size == problems)
      }
    }
  }
  /* "Show results" should {
    "display all the exceptions" in {
      (preliminaryBodyChecks ++ preliminaryFrontierChecks ++ preliminaryHeadChecks).filter(_.isFailure).foreach(_.failed.get.printStackTrace)
      assert(true)
    }
    "print all the results" in {
      (bodyChecks ++ frontierChecks ++ headChecks).foreach(println)
      assert(true)
    }
  }*/
}
