package eu.optique.jmora.mapping.analysis.test

import scala.language.implicitConversions
import eu.optique.api.component.omm.analysis.MappingAnalysisRelation
import eu.optique.api.component.omm.analysis.interfaces.optique.TerminalHelper
import java.io.File

object HelpingDefs {

  implicit def intToAss(i: Int) = AssertionNum(i)
  implicit def stringToAss(s: String) = AssertionID(s)
  implicit def UnqualifiedNamedProblem(unp: UnqualifiedNamedProblem)(implicit problems: Seq[String]) = unp.forElements("")

  private def niceprint(s: String) = println(s"    > $s")

  case class UnqualifiedProblem(n: Int, problem: MappingAnalysisRelation) {
    def forElements(elements: String)(implicit problems: Seq[String]): Boolean = {
      val msg = s"{TriplesMap$n} ${problem.message}${elements.split(" ").mkString("{", ", ", "}")}"
      niceprint(msg)
      problems contains msg
    }
  }

  case class AssertionNum(n: Int) {
    def hasProblem(problem: MappingAnalysisRelation) = UnqualifiedProblem(n, problem)
  }

  implicit def makeOntology(path: String) = (new TerminalHelper) getOntology path
  implicit def makeMappings(path: String) = (new TerminalHelper) getMappings path
  implicit def makeSchemata(path: String) = new File(path)

  case class AssertionID(name: String) {
    def hasProblem(problem: MappingAnalysisRelation) = UnqualifiedNamedProblem(name, problem)
  }

  case class UnqualifiedNamedProblem(name: String, problem: MappingAnalysisRelation) {
    def forElements(elements: String)(implicit problems: Seq[String]): Boolean = {
      val msg = s"{$name} ${problem.message} ${elements.split(" ").mkString("{", ", ", "}")}"
      niceprint(msg)
      problems contains msg
    }
  }

}
